package com.nurdicky.citraflorist.Api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by nurdicky on 01/10/17.
 */

public class Api {

    public static final String LOG = "CitraFlorist";

    public static final String URL = "http://192.168.43.117/florist/";
//    public static final String URL = "http://192.168.100.26/florist/";

//    public static final String BASE_URL = "http://192.168.100.26/florist/api/";
    public static final String BASE_URL = "http://192.168.43.117/florist/api/";

    private static Retrofit retrofit = null;


    public static Retrofit getClient() {
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

}
