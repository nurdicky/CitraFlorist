package com.nurdicky.citraflorist.Api;

import com.nurdicky.citraflorist.Model.CartResponse;
import com.nurdicky.citraflorist.Model.CategoryResponse;
import com.nurdicky.citraflorist.Model.ProductResponse;
import com.nurdicky.citraflorist.Model.UserResponse;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by nurdicky on 01/10/17.
 */

public interface ApiService {

    @FormUrlEncoded
    @POST("user/login")
    Call<UserResponse> login(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("user/register")
    Call<UserResponse> register(@FieldMap Map<String, String> params);

    @GET("product/category/{id}")
    Call<ProductResponse> productByCategory(@Path("id") String id);

    @GET("product/detail/{id}")
    Call<ProductResponse> detailProduct(@Path("id") String id);

    @GET("product/popular")
    Call<ProductResponse> popularProduct();

    @GET("category/list")
    Call<CategoryResponse> listCategory();

    @FormUrlEncoded
    @POST("cart/add")
    Call<CartResponse> addCart(@FieldMap Map<String, String> params);

   // @GET("cart/list")
    //Call<CartsResponse> listCart();

//    @FormUrlEncoded
//    @POST("cart/del")
//    Call<CartResponse> deleteCart(@Field("id_transaksi_sementara") String id);
//
//    @GET("kategori/list")
//    Call<KategoriResponse> listKategori();
//
//    @FormUrlEncoded
//    @POST("produk/search")
//    Call<ProductResponse> searchProduct(@Field("nama_produk") String nama_produk);
}
