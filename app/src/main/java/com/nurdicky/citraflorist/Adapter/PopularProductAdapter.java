package com.nurdicky.citraflorist.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nurdicky.citraflorist.Api.Api;
import com.nurdicky.citraflorist.Model.Product;
import com.nurdicky.citraflorist.R;
import com.nurdicky.citraflorist.View.Activity.DetailProductActivity;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;

/**
 * Created by nurdicky on 08/10/17.
 */

public class PopularProductAdapter extends RecyclerView.Adapter<PopularProductAdapter.ViewHolder> {

    private List<Product> productList;
    private Context context;

    public PopularProductAdapter(List<Product> productList, Context context) {
        this.productList = productList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.item_popular_product, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        final Product list = productList.get(position);

        final String harga = rupiah(Double.valueOf(list.getProductPrice()));

        holder.labelName.setText(list.getProductName());
        holder.labelPrice.setText(harga);

        final String path = Api.URL + list.getProductImage() ;
        Log.d(Api.LOG, "path image : "+ path);
        Picasso.with(context).load(path).fit().into(holder.imgProduct);

        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent().setClass(v.getContext().getApplicationContext(), DetailProductActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.putExtra("product_ID", list.getProductID());
                i.putExtra("product_name", list.getProductName());
                i.putExtra("product_price", harga);
                i.putExtra("product_image", path);
                v.getContext().startActivity(i);
                finish();
            }
        });

    }

    private void finish() {
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private LinearLayout container;
        private ImageView imgProduct;
        private TextView labelName, labelPrice;

        public ViewHolder(View itemView) {
            super(itemView);

            container = (LinearLayout) itemView.findViewById(R.id.container);
            imgProduct = (ImageView) itemView.findViewById(R.id.image_product);
            labelName = (TextView) itemView.findViewById(R.id.label_product_name);
            labelPrice = (TextView) itemView.findViewById(R.id.label_product_price);

        }
    }

    public String rupiah(double harga){
        DecimalFormat format = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("Rp. ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');

        format.setDecimalFormatSymbols(formatRp);
        return format.format(harga);
    }

}
