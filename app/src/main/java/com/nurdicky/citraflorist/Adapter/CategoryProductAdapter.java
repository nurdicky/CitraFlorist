package com.nurdicky.citraflorist.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nurdicky.citraflorist.Model.ProductCategory;
import com.nurdicky.citraflorist.R;
import com.nurdicky.citraflorist.View.Activity.ProductByCategoryActivity;

import java.util.List;

/**
 * Created by nurdicky on 08/10/17.
 */

public class CategoryProductAdapter extends RecyclerView.Adapter<CategoryProductAdapter.ViewHolder>{

    private List<ProductCategory> categoryList;
    private Context context;

    public CategoryProductAdapter(List<ProductCategory> categoryList, Context context) {
        this.categoryList = categoryList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_category_product, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        
        final ProductCategory list = categoryList.get(position);
        
        holder.labelName.setText(list.getCategoryName());
        
        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent().setClass(v.getContext().getApplicationContext(), ProductByCategoryActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.putExtra("category_ID", list.getCategoryID());
                i.putExtra("category_name", list.getCategoryName());
                v.getContext().startActivity(i);
                finish();
            }
        });
        
    }

    private void finish() {
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        
        private LinearLayout container;
        private TextView labelName;
        
        public ViewHolder(View itemView) {
            super(itemView);
            container = (LinearLayout) itemView.findViewById(R.id.container_category);
            labelName = (TextView) itemView.findViewById(R.id.label_category_name);
        }
    }
}
