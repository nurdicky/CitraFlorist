package com.nurdicky.citraflorist.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nurdicky.citraflorist.Api.Api;
import com.nurdicky.citraflorist.Model.Product;
import com.nurdicky.citraflorist.R;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;

/**
 * Created by nurdicky on 09/10/17.
 */

public class ProductCategoryAdapter extends RecyclerView.Adapter<ProductCategoryAdapter.ViewHolder>{

    private List<Product> productList;
    private Context context;


    public ProductCategoryAdapter(List<Product> productList, Context context) {
        this.productList = productList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_product_by_cateory, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Product list = productList.get(position);

        holder.labelName.setText(list.getProductName());
        final String harga = rupiah(Double.valueOf(list.getProductPrice()));

        holder.labelName.setText(list.getProductName());
        holder.labelPrice.setText(harga);

        final String path = Api.URL + list.getProductImage() ;
        Log.d(Api.LOG, "path image : "+ path);
        Picasso.with(context).load(path).fit().into(holder.imageProduct);

    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView labelName, labelPrice;
        private ImageView imageProduct;

        public ViewHolder(View itemView) {
            super(itemView);

            labelName = (TextView) itemView.findViewById(R.id.label_name_product);
            labelPrice= (TextView) itemView.findViewById(R.id.label_price_product);
            imageProduct = (ImageView) itemView.findViewById(R.id.image_product_category);

        }
    }

    public String rupiah(double harga){
        DecimalFormat format = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("Rp. ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');

        format.setDecimalFormatSymbols(formatRp);
        return format.format(harga);
    }
}
