package com.nurdicky.citraflorist.View.Activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.nurdicky.citraflorist.Api.Api;
import com.nurdicky.citraflorist.Api.ApiService;
import com.nurdicky.citraflorist.Model.Product;
import com.nurdicky.citraflorist.Model.ProductResponse;
import com.nurdicky.citraflorist.R;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nurdicky on 08/10/17.
 */

public class DetailProductActivity extends AppCompatActivity{


    private ImageView imgProduct;
    private TextView nameProduct, priceProduct, categoryProduct;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_detail_activity);

        imgProduct = (ImageView) findViewById(R.id.image_product_detail);
        nameProduct = (TextView) findViewById(R.id.label_detail_product_name);
        priceProduct = (TextView) findViewById(R.id.label_detail_product_price);
        categoryProduct = (TextView) findViewById(R.id.label_detail_product_category);

        nameProduct.setText(getIntent().getStringExtra("product_name"));
        priceProduct.setText(getIntent().getStringExtra("product_price"));

        Picasso.with(this).load(getIntent().getStringExtra("product_image")).fit().into(imgProduct);

        getProductDetail(getIntent().getStringExtra("product_ID"));
    }

    private void getProductDetail(String id) {

        ApiService service = Api.getClient().create(ApiService.class);

        Call<ProductResponse> call = service.detailProduct(id);

        call.enqueue(new Callback<ProductResponse>() {
            @Override
            public void onResponse(Call<ProductResponse> call, Response<ProductResponse> response) {
                if (response.isSuccessful()){

                    Product products = response.body().getProduct();

                    categoryProduct.setText(products.getProductCategory().getCategoryName());

                }
            }

            @Override
            public void onFailure(Call<ProductResponse> call, Throwable t) {

            }
        });
    }
}
