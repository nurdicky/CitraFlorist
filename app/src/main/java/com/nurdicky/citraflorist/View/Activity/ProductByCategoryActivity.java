package com.nurdicky.citraflorist.View.Activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.nurdicky.citraflorist.Adapter.ProductCategoryAdapter;
import com.nurdicky.citraflorist.Api.Api;
import com.nurdicky.citraflorist.Api.ApiService;
import com.nurdicky.citraflorist.Model.Product;
import com.nurdicky.citraflorist.Model.ProductResponse;
import com.nurdicky.citraflorist.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nurdicky on 09/10/17.
 */

public class ProductByCategoryActivity extends AppCompatActivity{

    private RecyclerView recyclerView;
    private ProductCategoryAdapter adapter;
    private List<Product> productList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_by_category_activity);
        setTitle(getIntent().getStringExtra("category_name"));

        recyclerView = (RecyclerView) findViewById(R.id.recyclerview_product_category);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(
                this, LinearLayoutManager.VERTICAL, false
        ));

        getProductByCategory(getIntent().getStringExtra("category_ID"));
    }

    private void getProductByCategory(String id) {

        ApiService service = Api.getClient().create(ApiService.class);

        Call<ProductResponse> call = service.productByCategory(id);

        call.enqueue(new Callback<ProductResponse>() {
            @Override
            public void onResponse(Call<ProductResponse> call, Response<ProductResponse> response) {
                if (response.isSuccessful()){

                    productList = response.body().getProducts();

                    adapter = new ProductCategoryAdapter(productList, getApplicationContext());
                    recyclerView.setAdapter(adapter);

                }
            }

            @Override
            public void onFailure(Call<ProductResponse> call, Throwable t) {
                Toast.makeText(ProductByCategoryActivity.this, "Failed load data!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
