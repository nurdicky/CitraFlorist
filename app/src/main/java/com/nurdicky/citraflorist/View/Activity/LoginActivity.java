package com.nurdicky.citraflorist.View.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.nurdicky.citraflorist.Api.Api;
import com.nurdicky.citraflorist.Api.ApiService;
import com.nurdicky.citraflorist.Model.UserResponse;
import com.nurdicky.citraflorist.R;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nurdicky on 01/10/17.
 */

public class LoginActivity extends AppCompatActivity {

    private EditText text_username, text_password;
    private Button btn_login;
    private TextView link_register;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        text_username = (EditText) findViewById(R.id.text_username);
        text_password = (EditText) findViewById(R.id.text_password);
        btn_login = (Button) findViewById(R.id.btn_login);
        link_register = (TextView) findViewById(R.id.link_register);

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String user_name = text_username.getText().toString();
                String user_pass = text_password.getText().toString();

                if (TextUtils.isEmpty(user_name)) {
                    text_username.setError("Field cannot be empty");
                    return;
                }else if (TextUtils.isEmpty(user_pass)){
                    text_password.setError("Field cannot be empty");
                    return;
                } else {
                    login(user_name, user_pass);
                }

            }
        });

        link_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });

    }

    private void login(String user_name, String user_pass) {

        Map<String, String> params = new HashMap<String, String>();
        params.put("username", user_name);
        params.put("password", user_pass);

        ApiService service = Api.getClient().create(ApiService.class);

        Call<UserResponse> call = service.login(params);

        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                Log.d(Api.LOG, "Success : "+ response.message());

                if (response.isSuccessful()){

                    Toast.makeText(LoginActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                }else{
                    Toast.makeText(LoginActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                Toast.makeText(LoginActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Log.e(Api.LOG, "Error : "+ t.getMessage());
            }
        });

    }
}
