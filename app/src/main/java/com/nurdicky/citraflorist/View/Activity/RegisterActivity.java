package com.nurdicky.citraflorist.View.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.nurdicky.citraflorist.Api.Api;
import com.nurdicky.citraflorist.Api.ApiService;
import com.nurdicky.citraflorist.Model.UserResponse;
import com.nurdicky.citraflorist.R;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nurdicky on 01/10/17.
 */

public class RegisterActivity extends AppCompatActivity {

    private EditText text_username, text_password, text_email;
    private Button btn_register;
    private TextView link_login;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        text_username = (EditText) findViewById(R.id.text_username);
        text_email = (EditText) findViewById(R.id.text_email);
        text_password = (EditText) findViewById(R.id.text_password);
        btn_register = (Button) findViewById(R.id.btn_register);
        link_login = (TextView) findViewById(R.id.link_login);

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String user_name = text_username.getText().toString();
                String user_mail = text_email.getText().toString();
                String user_pass = text_password.getText().toString();

                if (TextUtils.isEmpty(user_name)) {
                    text_username.setError("Field cannot be empty");
                    return;
                }else if (TextUtils.isEmpty(user_mail)){
                    text_password.setError("Field cannot be empty");
                    return;
                }else if (TextUtils.isEmpty(user_pass)){
                    text_password.setError("Field cannot be empty");
                    return;
                } else {
                    register(user_name, user_mail, user_pass);
                }

            }
        });

        link_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });


    }

    private void register(String user_name, String user_mail, String user_pass) {

        Map<String, String> params = new HashMap<String, String>();
        params.put("username", user_name);
        params.put("email", user_mail);
        params.put("password", user_pass);

        ApiService service = Api.getClient().create(ApiService.class);

        Call<UserResponse> call = service.register(params);

        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                Log.d(Api.LOG, "Success : "+ response.message());

                if (response.isSuccessful()){

                    if ( !response.body().getError() ){
                        Toast.makeText(RegisterActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                        startActivity(intent);
                    }else{
                        Toast.makeText(RegisterActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }else{
                    Toast.makeText(RegisterActivity.this, "Login Failed !", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                Toast.makeText(RegisterActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Log.e(Api.LOG, "Error : "+ t.getMessage());
            }
        });
    }
}
