package com.nurdicky.citraflorist.View.Fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.nurdicky.citraflorist.Adapter.CategoryProductAdapter;
import com.nurdicky.citraflorist.Adapter.PopularProductAdapter;
import com.nurdicky.citraflorist.Api.Api;
import com.nurdicky.citraflorist.Api.ApiService;
import com.nurdicky.citraflorist.Model.CategoryResponse;
import com.nurdicky.citraflorist.Model.Product;
import com.nurdicky.citraflorist.Model.ProductCategory;
import com.nurdicky.citraflorist.Model.ProductResponse;
import com.nurdicky.citraflorist.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nurdicky on 08/10/17.
 */

public class HomeFragment extends Fragment {

    private RecyclerView recyclerview;
    private PopularProductAdapter adapter;
    private CategoryProductAdapter adapterCategory;
    private List<Product> listProducts;
    private List<ProductCategory> listCategories;
    private RecyclerView recyclerviewCategory;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.home_fragment, container, false);

        recyclerview = view.findViewById(R.id.recyclerview_popular);
        recyclerviewCategory = view.findViewById(R.id.recyclerview_category);
        recyclerview.setHasFixedSize(true);
        recyclerviewCategory.setHasFixedSize(true);
        recyclerview.setLayoutManager(new LinearLayoutManager(
                getActivity(), LinearLayoutManager.HORIZONTAL, false
        ));
        recyclerviewCategory.setLayoutManager(new LinearLayoutManager(
                getActivity(), LinearLayoutManager.VERTICAL, false
        ));

        getPopularProduct();
        getCategoryProduct();

        return view;
    }

    private void getCategoryProduct() {

        ApiService service = Api.getClient().create(ApiService.class);

        Call<CategoryResponse> call = service.listCategory();

        call.enqueue(new Callback<CategoryResponse>() {
            @Override
            public void onResponse(Call<CategoryResponse> call, Response<CategoryResponse> response) {
                if (response.isSuccessful()){

                    listCategories = response.body().getProduct_category();

                    adapterCategory = new CategoryProductAdapter(listCategories, getActivity());
                    recyclerviewCategory.setAdapter(adapterCategory);

                }
            }

            @Override
            public void onFailure(Call<CategoryResponse> call, Throwable t) {
                Toast.makeText(getActivity(), "failed load data!", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void getPopularProduct() {

        ApiService service = Api.getClient().create(ApiService.class);

        Call<ProductResponse> call = service.popularProduct();

        call.enqueue(new Callback<ProductResponse>() {
            @Override
            public void onResponse(Call<ProductResponse> call, Response<ProductResponse> response) {
                if (response.isSuccessful()){

                    Log.d(Api.LOG, "response : " + response.body().getProduct() );

                    listProducts = response.body().getProducts();

                    adapter = new PopularProductAdapter(listProducts, getActivity());
                    recyclerview.setAdapter(adapter);


                }
            }

            @Override
            public void onFailure(Call<ProductResponse> call, Throwable t) {
                Toast.makeText(getActivity(), "Gagal load data!", Toast.LENGTH_SHORT).show();
            }
        });

    }


}
