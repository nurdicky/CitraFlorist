package com.nurdicky.citraflorist.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by nurdicky on 02/10/17.
 */

public class ProductCategory {

    @SerializedName("category_ID")
    @Expose
    private String categoryID;
    @SerializedName("category_name")
    @Expose
    private String categoryName;
    @SerializedName("category_type")
    @Expose
    private String categoryType;

    public String getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(String categoryID) {
        this.categoryID = categoryID;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(String categoryType) {
        this.categoryType = categoryType;
    }
}
