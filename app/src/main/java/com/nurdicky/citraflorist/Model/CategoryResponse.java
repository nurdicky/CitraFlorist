package com.nurdicky.citraflorist.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by nurdicky on 02/10/17.
 */

public class CategoryResponse {

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("product_category")
    @Expose
    private List<ProductCategory> product_category = null;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ProductCategory> getProduct_category() {
        return product_category;
    }

    public void setProduct_category(List<ProductCategory> product_category) {
        this.product_category = product_category;
    }
}
