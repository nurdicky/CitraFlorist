package com.nurdicky.citraflorist.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by nurdicky on 02/10/17.
 */

public class Product {

    @SerializedName("product_ID")
    @Expose
    private String productID;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("product_category")
    @Expose
    private ProductCategory productCategory;
    @SerializedName("product_price")
    @Expose
    private String productPrice;
    @SerializedName("product_description")
    @Expose
    private String productDescription;
    @SerializedName("product_image")
    @Expose
    private String productImage;
    @SerializedName("product_time_duration")
    @Expose
    private String productTimeDuration;
    @SerializedName("product_count_view")
    @Expose
    private String productCountView;

    public String getProductID() {
        return productID;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public ProductCategory getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(ProductCategory productCategory) {
        this.productCategory = productCategory;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public String getProductTimeDuration() {
        return productTimeDuration;
    }

    public void setProductTimeDuration(String productTimeDuration) {
        this.productTimeDuration = productTimeDuration;
    }

    public String getProductCountView() {
        return productCountView;
    }

    public void setProductCountView(String productCountView) {
        this.productCountView = productCountView;
    }
}
